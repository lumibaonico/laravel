@extends('layouts.app')
@section('title')
    Edit Student
@endsection
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    EDIT STUDENT 
                    <a href="{{url('student')}}">
                        <label class="back-btn btn btn-default btn-sm"> <- Back</label>
                    </a>
                </div>

                <div class="panel-body">

                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <form method="post" action="{{url('student/update')}}" enctype="multipart/form-data">
                        {{csrf_field()}}
                        <input type="hidden" name="id" value="{{$data['student']->id}}">
                        <input class="form-control input" type="text" name="firstname" value="{{$data['student']->firstname}}" placeholder="Enter Firstname">
                        <input class="form-control input" type="text" name="lastname" value="{{$data['student']->lastname}}" placeholder="Enter Lastname">
                        <input class="form-control input" type="text" name="email" value="{{$data['student']->email}}" placeholder="Enter Email">
                        <input class="form-control input" type="text" name="age" value="{{$data['student']->age}}" placeholder="Enter Age">
                        <select class="form-control input select_subject" name="subject[]" multiple="multiple">
                            @foreach($data['subjects'] as $subject)
                                <?php $selected = ''; ?>
                                @foreach($data['studentsubject'] as $subject_id)
                                    <?php
                                        if($subject_id == $subject->id){
                                            $selected = 'selected';
                                        }
                                    ?>
                                @endforeach
                                <option {{$selected}} value="{{$subject->id}}">{{$subject->subject_name}}</option>
                            @endforeach
                        </select>
                        <button class="form-control btn btn-success input">Save</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
$(document).ready(function(){

    $(".select_subject").select2();

});
</script>

@endsection


