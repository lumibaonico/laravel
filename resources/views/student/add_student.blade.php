@extends('layouts.app')
@section('title')
    Add Student
@endsection
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    ADD STUDENT 
                    <a href="{{url('student')}}">
                        <label class="back-btn btn btn-default btn-sm"> <- Back</label>
                    </a>
                </div>

                <div class="panel-body">

                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <form method="post" action="{{url('student/save')}}" enctype="multipart/form-data">
                        {{csrf_field()}}
                        <input class="form-control input" type="text" name="firstname" placeholder="Enter Firstname">
                        <input class="form-control input" type="text" name="lastname" placeholder="Enter Lastname">
                        <input class="form-control input" type="text" name="email" placeholder="Enter Email">
                        <input class="form-control input" type="text" name="age" placeholder="Enter Age">
                        <select class="form-control input select_subject" name="subject[]" multiple="multiple">
                            @foreach($subjects as $subject)
                                <option value="{{$subject->id}}">{{$subject->subject_name}}</option>
                            @endforeach
                        </select>
                        <button class="form-control btn btn-success input">Save</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
$(document).ready(function(){

    $(".select_subject").select2();

});
</script>

@endsection


