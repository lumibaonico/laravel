<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Student_subject;

class Student extends Model
{

    protected $table = 'students';
		  
	
	public function studentsubject(){
		
		return $this->hasOne('App\Student_subject','student_id','id');
	
	}


}
